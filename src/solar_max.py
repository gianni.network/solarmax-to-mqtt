#!/usr/bin/python
import paho.mqtt.client as mqtt
import os
import time
import subprocess
from solarmax import *

DEVICES = os.getenv('DEVICES')

mqtt_broker_ip = os.getenv("mqtt_broker_ip")
mqtt_broker_port = int(os.getenv("mqtt_broker_port"))
mqtt_username = os.getenv("mqtt_username")
mqtt_password = os.getenv("mqtt_password")
mqtt_topic = os.getenv("mqtt_topic")


class SMDev(object):
    def __init__(self, name, ipaddr, tcpport, address):
        self.name = name
        self.adr = address
        self.ipaddr = ipaddr
        self.tcpport = tcpport
        self.pac_now = 0
        self.kwh_today = 0
        self.data_today = []  # kWh, Wmax, h ?
        self.dd_slist = []  # last days production
        self.dm_slist = []  # last months production
        self.online = False


    def get_past_data(self, smc):
        smc.send(request_string(self.adr, 'DD01;DD02;DD03;DD04;DD05'))
        response = smc.receive()
        pstrli = response_to_pstringli(response)
        self.dd_slist = pstrli
        smc.send(request_string(self.adr, 'DD06;DD07;DD08;DD09;DD10'))
        response = smc.receive()
        pstrli = response_to_pstringli(response)
        self.dd_slist += pstrli
        smc.send(request_string(self.adr, 'DM01;DM02;DM03;DM04;DM05'))
        response = smc.receive()
        pstrli = response_to_pstringli(response)
        self.dm_slist = pstrli

    def get_current_data(self):
        try:
            spout = subprocess.check_output(["ping", "-c", "1", "-W", "1", self.ipaddr],
                                            stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError:
            print('Solarmax IP is not responding.')
            return {
                "name": self.name,
                "pac": float(0),
                "kwh_today": float(0),
                "online": False
            }

        try:
            smc = SMConn(self.ipaddr, self.tcpport)
            smc.send(request_string(self.adr, 'PAC;KDY;DD00'))
            response = smc.receive()
            smc.close()
            # print(response) # raw data string
            # pstrli = response_to_pstringli(response)
            pstrli = response_to_value(response)
            if len(pstrli) == 3:
                self.pac_now = pstrli[0]
                self.kwh_today = pstrli[1]
                self.data_today = pstrli[2][6:]
            self.online = True
        except Exception:
            self.online = False

    def print_current_data(self):
        print('Instant AC Power [W]: ' + str(self.pac_now))
        print('Energy today [kWh]: ' + str(self.kwh_today))
        print('Data today: ' + str(self.data_today))

    def print_current(self):
        print('-------------------------')
        print('Device: ' + self.name + ' (addr: ' + self.adr + ')')
        self.print_current_data()
        print('')

    def get_current_dict(self):
        cdict = {'name': self.name, 'addr': self.adr, 'pac': self.pac_now, 'kwh_today': self.kwh_today, 'online': self.online}
        return cdict


devs = []
for s in DEVICES.split(","):
    split = s.split(":")
    devs.append(SMDev(split[0], split[1], split[2], split[3]))

print("connecting to mqtt")
mqtt = mqtt.Client(client_id="solarmax-to-mqtt")
mqtt.username_pw_set(username=mqtt_username, password=mqtt_password)
mqtt.connect(host=mqtt_broker_ip, port=mqtt_broker_port)
print("mqtt connected")

print("starting main loop")
while True:
    values = []
    for dev in devs:
        dev.get_current_data()
        dev.print_current()
        values.append(dev.get_current_dict())

    final_sum = {
        "pac": float(0),
        "kwh_today": float(0)
    }
    all_online = True
    inverters_online = 0
    inverters_offline = 0
    for v in values:
        if not v['online']:
            print(f"{v['name']} is offline")
            all_online = False
            inverters_offline = inverters_offline + 1
        else:
            inverters_online = inverters_online + 1
        final_sum["pac"] = final_sum["pac"] + float(v["pac"])
        final_sum["kwh_today"] = final_sum["kwh_today"] + float(v["kwh_today"])

    current_total = round(float(final_sum["pac"]), 2)
    today_total = round(float(final_sum["kwh_today"]), 2)

    try:
        mqtt.publish(topic=f"{mqtt_topic}/current", payload=current_total)
        if all_online:
            mqtt.publish(topic=f"{mqtt_topic}/today", payload=today_total * 1000)
            mqtt.publish(topic=f"{mqtt_topic}/today_kwh", payload=today_total)
        else:
            print("Not pushing totals because not all inverters are online")
        mqtt.publish(topic=f"{mqtt_topic}/count_online", payload=inverters_online)
        mqtt.publish(topic=f"{mqtt_topic}/count_offline", payload=inverters_offline)
    except Exception as ex:
        template = "Publish.single: An exception of type {0} occured. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print(message)
        raise ex

    time.sleep(1)
