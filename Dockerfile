FROM python:3.9
RUN apt update && apt install -y iputils-ping
RUN pip3 install paho-mqtt
WORKDIR /app
COPY src/* /app/

ENTRYPOINT ["python", "/app/solar_max.py"]